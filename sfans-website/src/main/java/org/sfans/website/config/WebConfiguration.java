package org.sfans.website.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

@Profile("web")
@Configuration
public class WebConfiguration {

	@Autowired
	private ThymeleafViewResolver viewResolver;

	@Bean
	public ViewResolver delegatingViewResolver() {
		return new WebsiteViewResolver(viewResolver, viewResolver.getOrder() - 1);
	}

	@Profile("web")
	@Configuration
	protected static class WebConfig extends WebMvcConfigurerAdapter {
		@Autowired
		private WebsiteInterceptor websiteInterceptor;
		@Autowired
		private RedirectsInterceptor redirectsInterceptor;

		@Override
		public void addInterceptors(final InterceptorRegistry registry) {
			registry.addInterceptor(websiteInterceptor);
			registry.addInterceptor(redirectsInterceptor);
		}
	}

}
